import platform
import configparser

from selenium import webdriver

# Following are optional required
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import (
    ElementNotVisibleException
)
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup as bs
from urllib.request import (
    urlopen, urlparse, urlunparse, urlretrieve, Request)
from pathlib import Path
import numpy as np
from PIL import Image
import os
import sys
import requests
from requests.exceptions import ConnectionError
import time
import shutil
from datetime import datetime
from utils import create_folders

dt_start = datetime.now()

config = configparser.ConfigParser()
config.read('config.ini')

pressreader_config = config['PressReader']
username = pressreader_config['username']
password = pressreader_config['password']
out_folder = pressreader_config['out_folder'] if 'out_folder' in pressreader_config else 'images'

create_folders(out_folder, is_file=False)


def downloadimage(url, outpath):
    print('Url: ' + url + ' ,Outpath: ' + outpath)
    # Insert a timer for avoid banners
    time.sleep(0.1)
    # Download de image with path out_file
    response = requests.get(url, stream=True)
    response.raw.decode_content = True
    with open(outpath, 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    return response


listbaseurls = ['https://www.pressreader.com/argentina/clarin/20190805',
                'https://www.pressreader.com/argentina/la-nacion/20190805',
                'https://www.pressreader.com/argentina/la-voz-del-interior/20190805',
                'https://www.pressreader.com/argentina/ole/20190805',
                'https://www.pressreader.com/argentina/perfil-domingo/20190804',
                'https://www.pressreader.com/el-salvador/el-grafico/20190805',
                'https://www.pressreader.com/argentina/el-cronista-comercial/20190805',
                'https://www.pressreader.com/argentina/los-andes/20190805',
                'https://www.pressreader.com/argentina/el-dia-la-plata/20190805']

# Create instance of webdriver
operating_system = platform.system()
if operating_system not in ['Linux', 'Windows']:
    raise ValueError(f'Error: Sistema opertivo {operating_system} no soportado')

path_driver = 'resources/linux/chromedriver' if operating_system == 'Linux' \
    else 'resources/windows/chromedriver.exe'

mydriver = webdriver.Chrome(path_driver)
for baseurl in listbaseurls:
    print('Execute: ' + baseurl)
    mydriver.maximize_window()

    time.sleep(3)
    mydriver.get(baseurl)

    #Generate mouse accion over my driver
    actions = ActionChains(mydriver)

    # Wait until the entire div get charged
    WebDriverWait(mydriver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, "toolbar-button-signin")))

    #Click in button signin
    mydriver.find_element_by_class_name("toolbar-button-signin").click()

    #Clear Username TextBox if already allowed "Remember Me"
    mydriver.find_element_by_id('SignInEmailAddress').clear()

    #Write Username in Username TextBox
    mydriver.find_element_by_id('SignInEmailAddress').send_keys(username)

    #Clear Password TextBox if already allowed "Remember Me"
    mydriver.find_element_by_xpath("//input[@type='password']").clear()

    #Write Password in password TextBox
    mydriver.find_element_by_xpath("//input[@type='password']").send_keys(password)

    #Click Login button
    mydriver.find_element_by_xpath("//button[@type='submit']").click()

    # Wait until the articles toolbar have been charged
    WebDriverWait(mydriver, 1000).until(EC.presence_of_element_located((By.ID, "thumbsToolbarBottom_0")))

    libuttons = mydriver.find_elements_by_xpath("//li[contains(@id, 'thumbsToolbarBottom')]")

    # Get all images of the page
    images = mydriver.find_elements_by_tag_name('img')

    # Initialize the page number
    pagenum = 0

    time.sleep(10) #with time.sleep(5) works correctly

    # Filter icons and profile images
    maxscale = 0
    min_scale = 1000000
    for image in images:
        genurl = image.get_attribute('src')
        if str(genurl).find("1x1") == -1 and str(genurl).find("user-pictures") == -1 and str(genurl).find("290") == -1:
            scale = genurl[genurl.find("scale=") + 6:]
            if scale.find("&") != -1:
                scale = int(scale[:scale.find("&")])
            else:
                scale = int(scale)

            if scale > maxscale:
                maxscale = scale
            if scale < min_scale:
                min_scale = scale

    # it works for know if the image has layer and if it has starts with layer fg
    scale = genurl[genurl.find("scale=") + 6:]
    if scale.find("layer") != -1:
        layer = "&layer=fg"
        layerurl = "_fg"
    else:
        layer = ""
        layerurl = ""

    template_base_url = genurl[:genurl.find("page=") + 5] + \
                        '{}&' + genurl[genurl.find("scale="):genurl.find("scale=") + 6] + \
                        '{}' + layer

    while True:
        all_page_complete = False
        short_iteration = True
        # inc pagenum
        pagenum = pagenum + 1

        # Get the Layer of image (if exists)
        # if genurl.find("layer=") != -1:
        #     layerurl = "_fg"
        # else:
        #     layerurl = ""

        # Build the outpath
        filename = baseurl.split("/")[-2] + "_" + baseurl.split("/")[-1] + "_" + str(pagenum) + layerurl + '.jpg'
        outpath = os.path.join(out_folder, filename)

        #for variationscale in range(maxscale + 100, min_scale - 1, -1):
        prom_scale=84
        for variationscale in range(0,20):
            # https://i.prcdn.co/img?file=e8662019080500000000001001&page=37&scale=55&layer=fg

            sum_scale = prom_scale + variationscale
            res_scale= prom_scale - variationscale

            spec_sum_url = template_base_url.format(pagenum, sum_scale)

            spec_res_url = template_base_url.format(pagenum, res_scale)

            response_res = downloadimage(spec_res_url, outpath)

            response_sum = downloadimage(spec_sum_url, outpath)

            try:
                response_res.raise_for_status()
                if response_res.status_code == 200:
                    short_iteration = False
                    break
            except requests.exceptions.HTTPError as e:
                os.remove(Path(outpath))

            try:
                response_sum.raise_for_status()
                if response_sum.status_code == 200:
                    short_iteration = False
                    break
            except requests.exceptions.HTTPError as e:
                os.remove(Path(outpath))

        if short_iteration:
            for variationscale in range(maxscale + 100, min_scale - 1, -1):
                # https://i.prcdn.co/img?file=e8662019080500000000001001&page=37&scale=55&layer=fg
                specurl = template_base_url.format(pagenum, variationscale)

                response = downloadimage(specurl, outpath)
                try:
                    response.raise_for_status()
                    if response.status_code == 200:
                        break
                except requests.exceptions.HTTPError as e:
                    os.remove(Path(outpath))
                    # if the page don't exists come in the condicional
                    if response.status_code == 404:
                        print('break for: ' + specurl)
                        all_page_complete = True
                        break

        if all_page_complete:
            break

        # If the image is composed of 2 images then joined the images
        if specurl.find("layer=") != -1:
            if specurl[specurl.find("layer=") + 6:] == "fg":
                filefg = Path(outpath)
                filebg = Path(outpath.replace("fg", "bg"))
                # time.sleep(0.1)
                if filefg.is_file():
                    specurl = specurl.replace("fg", "bg")
                    downloadimage(specurl, outpath.replace("fg", "bg"))
                    try:
                        response.raise_for_status()
                    except requests.exceptions.HTTPError as e:
                        os.remove(Path(outpath))
                        print('break for: ' + specurl)
                        break

            if filefg.is_file() and filebg.is_file():
                imgfg = Image.open(filefg).convert("RGBA")
                imgbg = Image.open(filebg).convert("RGBA")

                filenameConvinated = baseurl.split("/")[-2] + "_" + baseurl.split("/")[-1] + "_" + str(pagenum) + '.png'
                outpathConvinated = os.path.join(out_folder, filenameConvinated)

                Image.alpha_composite(imgbg, imgfg).save(outpathConvinated)

                os.remove(filefg)
                os.remove(filebg)

    print('Finished with: ' + baseurl)

# Close the navigator
#mydriver.close()
dt_end = datetime.now()
print (dt_end - dt_start)