from selenium import webdriver

#Following are optional required
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import (
    ElementNotVisibleException
)
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup as bs
from urllib.request import (
    urlopen, urlparse, urlunparse, urlretrieve, Request)
from pathlib import Path
import numpy as np
from PIL import Image
import os
import sys
import requests
import time
import shutil

def DownloadImages(baseurl,url):
    time.sleep(0.1)

    # Get the number of page
    page = url[url.find("page=") + 5:]
    page = page[:page.find("&")]

    # Get the Layer of image (if exists)
    if url.find("layer=") != -1:
        layer = "_" + url[url.find("layer=") + 6:]
    else:
        layer = ""

    # Build the outpath
    filename = baseurl.split("/")[-2] + "_" + baseurl.split("/")[-1] + "_" + page + layer + '.jpg'
    outpath = os.path.join(out_folder, filename)

    # Download de image with path out_file
    response = requests.get(url, stream=True)
    response.raw.decode_content = True
    with open(outpath, 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response

    #time.sleep(1)

    # If the image is composed of 2 images then joined the images
    if url.find("layer=") != -1:
        if url[url.find("layer=") + 6:] == "bg":
            filebg = Path(outpath)
            filefg = Path(outpath.replace("bg", "fg"))
        if url[url.find("layer=") + 6:] == "fg":
            filefg = Path(outpath)
            filebg = Path(outpath.replace("fg", "bg"))

        if filefg.is_file() and filebg.is_file():
            imgfg = Image.open(filefg).convert("RGBA")
            imgbg = Image.open(filebg).convert("RGBA")

            filenameConvinated = baseurl.split("/")[-2] + "_" + baseurl.split("/")[-1] + "_" + page + '.png'
            outpathConvinated = os.path.join(out_folder, filenameConvinated)

            Image.alpha_composite(imgbg, imgfg).save(outpathConvinated)

            os.remove(filefg)
            os.remove(filebg)


    #time.sleep(1)



#baseurl = "https://www.pressreader.com/uk/wallpaper/20190701"
#baseurl ="https://www.pressreader.com/argentina/caras/20190724"
username = "Carolina.Lombardi@umww.com"
password = "costarica6012"
listbaseurls=['https://www.pressreader.com/argentina/clarin/20190805',
              'https://www.pressreader.com/argentina/la-nacion/20190805',
              'https://www.pressreader.com/argentina/la-voz-del-interior/20190805',
              'https://www.pressreader.com/argentina/ole/20190805',
              'https://www.pressreader.com/argentina/perfil-domingo/20190804',
              'https://www.pressreader.com/el-salvador/el-grafico/20190805',
              'https://www.pressreader.com/argentina/el-cronista-comercial/20190805',
              'https://www.pressreader.com/argentina/los-andes/20190805',
              'https://www.pressreader.com/argentina/el-dia-la-plata/20190805']
for baseurl in listbaseurls:
    mydriver = webdriver.Chrome(executable_path='C:/Users/sebastian.teplitzky/Downloads/chromedriver_win32/chromedriver.exe')
    mydriver.get(baseurl)
    mydriver.maximize_window()

    #Generate mouse accion over my driver
    actions = ActionChains(mydriver)

    #Wait until the entire div get charged
    WebDriverWait(mydriver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, "toolbar-button-signin")))

    #Click in button signin
    mydriver.find_element_by_class_name("toolbar-button-signin").click()

    #Clear Username TextBox if already allowed "Remember Me"
    mydriver.find_element_by_id('SignInEmailAddress').clear()

    #Write Username in Username TextBox
    mydriver.find_element_by_id('SignInEmailAddress').send_keys(username)

    #Clear Password TextBox if already allowed "Remember Me"
    mydriver.find_element_by_xpath("//input[@type='password']").clear()

    #Write Password in password TextBox
    mydriver.find_element_by_xpath("//input[@type='password']").send_keys(password)

    #Click Login button
    mydriver.find_element_by_xpath("//button[@type='submit']").click()

    #Cambiar
    WebDriverWait(mydriver, 1000).until(EC.presence_of_element_located((By.ID, "thumbsToolbarBottom_0")))

    libuttons = mydriver.find_elements_by_xpath("//li[contains(@id, 'thumbsToolbarBottom')]")

    #if Flag is true then i got back to the first article
    Flag=False

    #Output folder
    out_folder="C:/Users/sebastian.teplitzky/Desktop/dwnl/"

    while True:

        #Get the selected TAG
        lisel=mydriver.find_elements_by_xpath("//li[contains(@id, 'thumbsToolbarBottom') and contains(@class,'sel')]")
        if lisel.__len__() > 0:
            id=lisel[0].get_attribute("id")
            itemid=str(id)[id.find("_")+1:]

        #Check if i got back to the first article then break of the cicle
        if lisel.__len__()>0:
            if itemid< str(libuttons.__len__() - 1) and Flag:
                break

        #Wait until the div is enabled
        WebDriverWait(mydriver, 1).until(EC.presence_of_element_located((By.CLASS_NAME, "rn-right")))

        #Get all images of the page
        images = mydriver.find_elements_by_tag_name('img')
        for image in images:

            url = image.get_attribute('src')

            #Filter icons and profile images
            if str(url).find("1x1") == -1 and str(url).find("user-pictures")== -1 and str(url).find("290")== -1:
                DownloadImages(baseurl,url)

        # if we are in the last article then is set the flag in true
        if lisel.__len__() > 0:
            if itemid==str(libuttons.__len__() - 1):
                Flag=True

        #Move to the next page
        actions.move_to_element(mydriver.find_element_by_class_name("rn-right")).click().perform()

    mydriver.close()


