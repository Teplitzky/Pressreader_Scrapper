from selenium import webdriver

#Following are optional required
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import (
    ElementNotVisibleException
)
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup as bs
from urllib.request import (
    urlopen, urlparse, urlunparse, urlretrieve, Request)
from pathlib import Path
import numpy as np
from PIL import Image
import os
import sys
import requests
import time
import shutil
out_folder="C:/Users/sebastian.teplitzky/Desktop/dwnl/"
baseurl ="https://www.pressreader.com/argentina/caras/20190724"
#list=['https://i.prcdn.co/img?file=e4442019072400000000001001&page=1&scale=57','https://i.prcdn.co/img?file=e4442019072400000000001001&page=13&scale=57','https://i.prcdn.co/img?file=eaax2019080500000000001001&page=7&scale=78&layer=bg','https://i.prcdn.co/img?file=eaax2019080500000000001001&page=7&scale=78&layer=fg']
#list=['https://i.prcdn.co/img?file=22602019080600000000001001&page=1&scale=55&layer=bg','https://i.prcdn.co/img?file=22602019080600000000001001&page=1&scale=55&layer=fg']
list=['https://i.prcdn.co/img?file=22602019080600000000001001&page=2&scale=55&layer=bg','https://i.prcdn.co/img?file=22602019080600000000001001&page=2&scale=55&layer=fg']
for url in list:
    time.sleep(1)

    #Get the number of page
    page= url[url.find("page=")+5:]
    page=page[:page.find("&")]

    #Get the Layer of image (if exists)
    if url.find("layer=") != -1:
        layer="_" + url[url.find("layer=")+6:]
    else:
        layer=""

    #Build the outpath
    filename = baseurl.split("/")[-2] + "_" + baseurl.split("/")[-1] + "_" + page + layer + '.jpg'
    outpath = os.path.join(out_folder, filename)
    #req = Request(url,headers={'User-Agent': 'Chrome/70.0.3538.77'})


    #Download de image with path out_file
    response = requests.get(url, stream=True)
    response.raw.decode_content = True
    with open(outpath, 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response

    #time.sleep(1)
    #webpage = urlopen(req).read()
    time.sleep(1)

    #If the image is composed of 2 images then joined the images
    if url.find("layer=") != -1:
        if url[url.find("layer=") + 6:] == "bg":
            filebg = Path(outpath)
            filefg = Path(outpath.replace("bg","fg"))
        if url[url.find("layer=") + 6:] == "fg":
            filefg = Path(outpath)
            filebg = Path(outpath.replace("fg","bg"))

        if filefg.is_file() and filebg.is_file():
            imgfg= Image.open(filefg).convert("RGBA")
            imgbg = Image.open(filebg).convert("RGBA")


            filenameConvinated = baseurl.split("/")[-2] + "_" + baseurl.split("/")[-1] + "_" + page + '.png'
            outpathConvinated = os.path.join(out_folder, filenameConvinated)

            Image.alpha_composite(imgbg,imgfg).save(outpathConvinated)
            #img.save(outpathConvinated)


    time.sleep(1)
    #open(outpath, 'wb').write(webpage)