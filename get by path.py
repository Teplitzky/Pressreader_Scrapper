import http.client, urllib.request, urllib.parse, urllib.error, base64

headers = {
    # Request headers
    'Ocp-Apim-Subscription-Key': '{subscription key}',
}

params = urllib.parse.urlencode({
    # Request parameters
    'accessToken': '{string}',
    'pathUri': '{string}',
    'prefer': 'Category',
})

try:
    conn = http.client.HTTPSConnection('api.pressreader.com')
    conn.request("GET", "/user/catalog/v1/navigationpath?%s" % params, "{body}", headers)
    response = conn.getresponse()
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))