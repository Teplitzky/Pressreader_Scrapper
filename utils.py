import os
import PIL
import socket
import logging
import calendar
import platform
import mimetypes

# from PIL import Image
# from resizeimage import resizeimage
from datetime import datetime, timedelta


class LoggingType:
    CRITICAL = 50
    ERROR = 40
    WARNING = 30
    INFO = 20
    DEBUG = 10
    EXCEPTION = -10

operating_system = platform.system()

# funcion para validar paths
def is_path_exists_or_creatable(pathname):
    if os.path.exists(pathname):
        if os.access(pathname, os.W_OK):
            return True
        else:
            return False
    else:
        dirname = os.path.dirname(pathname) or os.getcwd()
        return os.access(dirname, os.W_OK)


def include_file_name(pathname):
    return pathname == os.path.dirname(pathname)


def get_relation_one_to_many(records, key_one, key_many, label_key_one, label_key_many, exclude_one=None,
                             exclude_many=None, exclude_ones_without_relations=False):
    """De la lista records devuelve la lista one_to_many que expresa la relacion de 1 a N de records entre el valor
       referenciado por la clave key_one y los valores referenciados por la clave key_to_many

       Keyword arguments:
       records: lista de objetos
       key_one: clave que representa el campo 1 de la relacion 1 a N
       key_many: clave que representa el campo N de la relacion 1 a N
       label_key_one: valor de la clave del campo 1 en la lista one_to_many
       label_key_many: valor de la clave del campo N en la lista one_to_many
       exclude_one: lista con valores de la calve principal a excluir de ones_to_many
       exclude_many: lista con valores de la clave secundaria a excluir de ones_to_many
    """

    one_to_many = []
    record_one_to_many = {}
    record_one_to_many[label_key_one] = ''
    record_one_to_many[label_key_many] = []
    if not exclude_one:
        exclude_one = []
    if not exclude_many:
        exclude_many = []

    ones_added = set()
    ones_add = ones_added.add
    # esta linea obtiene una lista con los valores para key_one evitando tener duplicados y sin perder el orden.
    ones = [record[key_one] for record in records if not (record[key_one] in ones_added or ones_add(record[key_one]))]

    for one in ones:
        if not one in exclude_one:
            record_one_to_many[label_key_one] = one
            record_one_to_many[label_key_many] = []

            for record in records:
                if one == record[key_one] and not (record[key_many] in exclude_many):
                    record_one_to_many[label_key_many].append(record[key_many])

            if exclude_ones_without_relations:
                if len(record_one_to_many[label_key_many]) > 0:
                    one_to_many.append(record_one_to_many.copy())

            else:
                one_to_many.append(record_one_to_many.copy())

    return one_to_many


def myconverter(o):
    if isinstance(o, datetime):
        return o.__str__()


def differentiate_ambiguous_fields(fields_ad_objects):
    """
        Abstract: Diferencia los campos pasados que puedan resultar ambiguos en relacion a las listas pasadas
                  agregando, de ser necesario, como prefijo el nombre del objeto mas '_' y los devuelve en
                  un diccionario de dos niveles donde en el segundo la clave es el nombre viejo del campo
                  y el valor es el nombre nuevo.
                  En caso de estar especificado un prefijo este se utilizara en lugar del prefijo por default
                  de ser necesario, a menos que este seteado en true always_prefix, en tal caso el prefijo
                  se aplicara en todos los casos. La logica es la misma para el sufijo.

        Keyword arguments:
            fields_ad_objects = {
                    'ad_object_1' : [str] / {'fields': [str], 'always_prefix': boolean, 'prefix': str, 'always_sufix': boolean, 'sufix': str},
                    'ad_object_N': [str] / {'fields': [str], 'always_prefix': boolean, 'prefix': str, 'always_sufix': boolean, 'sufix': str}
                }

        Return: {
                    'ad_object_1': {
                        'old_name_field_1' : new_name_field_1,
                        'old_name_field_N': new_name_field_N
                    },
                    'ad_object_N': {
                        'old_name_field_1' : new_name_field_1,
                        'old_name_field_N': new_name_field_N
                    }
                }

    """

    differentiate_fields = {}
    fields_ambiguous = []
    keys_to_search = fields_ad_objects.keys()
    for ad_object in fields_ad_objects:
        differentiate_fields[ad_object] = {}
        if isinstance(fields_ad_objects[ad_object], list):
            fields_adobject = fields_ad_objects[ad_object]
            prefix = ad_object + "_"
            always_prefix = False
            sufix = ''
            always_sufix = False
        else:
            fields_adobject = fields_ad_objects[ad_object]['fields']
            prefix = fields_ad_objects[ad_object]['prefix']
            always_prefix = fields_ad_objects[ad_object]['always_prefix']
            sufix = fields_ad_objects[ad_object]['sufix']
            always_sufix = fields_ad_objects[ad_object]['always_sufix']
        for field in fields_adobject:
            is_ambiguous = False
            new_field = prefix + field + sufix

            if not field in fields_ambiguous:
                for key in keys_to_search:
                    if not is_ambiguous:
                        if key != ad_object:
                            if field in fields_ad_objects[key]:
                                is_ambiguous = True
                if is_ambiguous:
                    differentiate_fields[ad_object][field] = new_field
                    fields_ambiguous.append(field)
                else:
                    default_field = field
                    if always_prefix:
                        default_field = prefix + default_field
                    if always_sufix:
                        default_field = default_field + sufix
                    differentiate_fields[ad_object][field] = default_field
            else:
                differentiate_fields[ad_object][field] = new_field

    return differentiate_fields


def copy_dict_changing_keys(data, oldkeys_newkeys):
    copy = {}
    for d in data:
        if d in oldkeys_newkeys:
            copy[oldkeys_newkeys[d]] = data[d]

    return copy


def restart_daily_logging_file(datetime_log, template_path_file, format_datetime='%Y%m%d_h%Hm%M',
                               formatter_fmt='%(asctime)s - %(message)s', dedicated_error_log=True,
                               initial_message=''):
    now = datetime.now()
    if now.day != datetime_log.day:
        datetime_log = now
        # log_general_path = template_path_file.format(datetime_log.strftime(format_datetime))
        log_general_path = template_path_file \
            .format(datetime_log.year,
                    str(datetime_log.month).zfill(2),
                    datetime_log.strftime('%Y%m%d_h%Hm%M'))
        create_folders(log_general_path)
        print_and_logging('Generando nuevo archivo de logging general: {}'.format(log_general_path))
        if dedicated_error_log:
            log_error_path = log_general_path.replace('info', 'error')
            log_error_path = log_error_path.replace('.log', '_ERROR.log')
            # log_error_path = log_general_path.replace('.log', '_ERROR.log')
            create_folders(log_error_path)
            print_and_logging('Generando nuevo archivo de logging de errores: {}'.format(log_error_path))
        print_and_logging('Fin de archivo.')

        file_handler = logging.FileHandler(log_general_path, 'w')
        file_handler.setLevel(logging.INFO)
        formatter = logging.Formatter(formatter_fmt)
        file_handler.setFormatter(formatter)
        log = logging.getLogger()
        handlers = log.handlers.copy()
        for handler in handlers:
            log.removeHandler(handler)
        log.addHandler(file_handler)

        if dedicated_error_log:
            # archivo de log de errores
            file_handler_error = logging.FileHandler(log_error_path, 'w')
            file_handler_error.setLevel(logging.ERROR)
            formatter = logging.Formatter(formatter_fmt)
            file_handler_error.setFormatter(formatter)
            log.addHandler(file_handler_error)

            print_and_logging(initial_message)

    return datetime_log


def print_and_logging(message, logging_type: LoggingType = LoggingType.INFO):
    print(message)
    if logging_type == LoggingType.INFO:
        logging.info(message)
    else:
        if logging_type == LoggingType.DEBUG:
            logging.debug(message)
        else:
            if logging_type == LoggingType.WARNING:
                logging.warning(message)
            else:
                if logging_type == LoggingType.ERROR:
                    logging.error(message)
                else:
                    if logging_type == LoggingType.CRITICAL:
                        logging.critical(message)
                    else:
                        if logging_type == LoggingType.EXCEPTION:
                            logging.exception(message)
                        else:
                            logging.info(message)


def print_start_time(message='Start time {}', format_time='%Y-%m-%d %H:%M:%S.%f', to_log=False):
    start_time = datetime.strptime(datetime.now().strftime(format_time), format_time)
    literal_format_time = '{:' + format_time + '}'
    if to_log:
        logging.info(message.format(literal_format_time.format(start_time)))
    print(message.format(literal_format_time.format(start_time)))

    return start_time


def print_end_elapsed_time(start_time, format_time='%Y-%m-%d %H:%M:%S.%f', message='End time: {}   Elapsed time: {}',
                           to_log=False):
    end_time = datetime.strptime(datetime.now().strftime(format_time), format_time)
    literal_format_time = '{:' + format_time + '}'
    elapsed_time = end_time - start_time
    if to_log:
        logging.info(message.format(literal_format_time.format(end_time), elapsed_time))
    if message is not None:
        print(message.format(literal_format_time.format(end_time), elapsed_time))

    return elapsed_time


def get_start_yesterday():
    today = datetime.strptime(datetime.strftime(datetime.now(), '%d-%m-%Y'), '%d-%m-%Y')
    yesterday = today - timedelta(days=1)

    return yesterday


def get_start_month(date=datetime.now()):
    return datetime(date.year, date.month, 1)


def get_end_month(date=datetime.now()):
    date_timetuple = date.timetuple()
    date_end_month = datetime(date_timetuple[0], date_timetuple[1],
                              calendar.monthrange(date_timetuple[0], date_timetuple[1])[1])

    return date_end_month


def get_start_next_month(date=datetime.now()):
    date_end_month = get_end_month(date)
    start_next_month = date_end_month + timedelta(days=1)

    return start_next_month


# def fit_image(path_image, path_save, width, height):
#
#     with open(path_image, 'r+b') as f:
#         with Image.open(f) as image:
#             fited = resizeimage.resize_contain(image, [width, height])
# #             print(image.format)
#             fited.convert('RGB').save(path_save, image.format)
# #             fited.save(path_save)


def fit_resize_image(path_image, path_save, width, height):
    img = Image.open(path_image)
    width_original = img.size[0]
    height_original = img.size[1]
    if width < img.size[0] and height < img.size[1]:

        width_difference = (width / float(img.size[0]))
        height_difference = (height / float(img.size[1]))
        if width_difference >= height_difference:
            final_height = int((float(img.size[1]) * float(width_difference)))
            img = img.resize((width, final_height), PIL.Image.ANTIALIAS)
        else:
            final_width = int((float(img.size[0]) * float(height_difference)))
            img = img.resize((final_width, height), PIL.Image.ANTIALIAS)

        print(width_original, height_original)
        print(img.size[0], img.size[1], '\n')
    else:
        print('No entro')
        print(width_original, height_original, '\n')


#         img.convert('RGB').save(path_save, img.format)


def fit_two_resize(path_image, path_save, width, height):
    resized = False
    img = Image.open(path_image)
    print(img.size[0], img.size[1])
    #     width_original = img.size[0]
    #     height_original = img.size[1]
    if width < img.size[0]:
        img = resizeimage.resize_width(img, width)
        print(img.size[0], img.size[1])
        if height < img.size[1]:
            img = resizeimage.resize_height(img, height)
            print(img.size[0], img.size[1])
        resized = True

    if height < img.size[1]:
        img = resizeimage.resize_height(img, height)
        print(img.size[0], img.size[1])
        if width < img.size[0]:
            img = resizeimage.resize_width(img, width)
            print(img.size[0], img.size[1])
        resized = True

    if resized:
        img.convert('RGB').save(path_save, img.format)


def fit_images_folder(path_images, path_save, width, height):
    images = os.listdir(path_images)
    for image in images:
        content_type = mimetypes.guess_type(image)[0]
        content_type = 'binary/octet-stream' if content_type is None else content_type
        if 'image' in content_type:
            fit_two_resize(path_images + '\\' + image, path_save + '\\' + image, width, height)


def loggin_exception(exception, traceback, label='File {}, line {}, in {}'):
    logging.error('Exception: {}'.format(exception))
    logging.error(label.format(traceback.tb_frame.f_code.co_filename,
                               traceback.tb_frame.f_code.co_firstlineno,
                               traceback.tb_frame.f_code.co_name))
    traceback_next = traceback.tb_next
    while traceback_next is not None:
        logging.error(label.format(traceback_next.tb_frame.f_code.co_filename,
                                   traceback_next.tb_frame.f_code.co_firstlineno,
                                   traceback_next.tb_frame.f_code.co_name))
        traceback_next = traceback_next.tb_next


def create_folders(path, is_file=True):
    like_unix = True if operating_system in ['Linux', 'Darwin'] else False
    separator = '/' if like_unix else '\\'
    path_base = '' if os.path.isabs(path) else os.getcwd()

    actually_path = os.path.join(path_base, os.path.dirname(path)) if is_file else os.path.join(path_base, path)
    folders = [f for f in actually_path.split(separator) if f != '']

    _path_output = separator if like_unix else ''
    for folder in folders:
        _path_output = os.path.join(_path_output, folder)
        if not os.path.isdir(_path_output):
            os.mkdir(_path_output)

    return _path_output


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()

    return ip
