import os
import shutil
extensions = {".jpg", ".png", ".gif"}
src=("C:\\Users\sebastian.teplitzky\\Desktop\\src")
src_files = os.listdir(src)
for file_name in src_files:
    full_file_name = os.path.join(src, file_name)
    for ext in extensions:
        if os.path.isfile(full_file_name) and file_name.endswith(ext):
            shutil.copy(full_file_name, "C:\\Users\sebastian.teplitzky\\Desktop\\dest")